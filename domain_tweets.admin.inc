<?php

function domain_tweets_view_all_form() {

  $query_list_query = db_query('
      SELECT DISTINCT query_id 
      FROM {domain_tweets}
      ORDER BY query_id;');

  while ($result = db_fetch_array($query_list_query)) {
    $form_name = 'form'.$result['query_id'];
    $form[$form_name] = _domain_tweets_get_query_edit_form($result['query_id'], TRUE);

  }

  /*BUG: The Global Settings fieldset can't be above the edit forms or the
   *  first edit form's submit button will behave indistinguishably from the
   *  global settings' submit button.  
   */
  $form['global_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Global Settings'),
      '#collapsible' => TRUE,
      );
  $form['global_settings']['disable'] = array(
      '#type' => 'checkbox',
      '#default_value' => variable_get('domain_tweets_disable', FALSE),
      '#title' => t('Disable Domain Tweets for all neighborhoods?'),
      );
  $form['global_settings']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#submit' => array('domain_tweets_disable_all_submit'),
      );

  $form['new'] = array(
      '#type' => 'submit',
      '#value' => 'Create a new query.',
      '#submit' => array('domain_tweets_view_all_form_submit'),
      );

  return $form;
}


function domain_tweets_edit_form($form, $query_id) {
  $form = array();
  if (_domain_tweets_query_id_is_valid($query_id) || ($query_id == 'new')) {
    $form['edit'] = _domain_tweets_get_query_edit_form($query_id);
  }
  return $form;
}


function domain_tweets_delete_form(&$form_state, $query_id = -1) {

  if (!_domain_tweets_query_id_is_valid($query_id) || $query_id == 0) {
    drupal_set_message(t('You have attempted to delete a query that doesn\'t exist.  Don\'t do that.'), 'error');
    drupal_goto('admin/settings/domain_tweets');
  }

  $form['query_id'] = array(
      '#type' => 'value',
      '#value' => $query_id,
      );

  $name_result = db_result(
      db_query('SELECT query_name
        FROM {domain_tweets}
        WHERE query_id = %d', $query_id));

  return confirm_form($form, t('Are you sure you want delete %name?',
        array('%name' => $name_result)),
      'admin/settings/domain_tweets',
      NULL, t('Delete'), t('Cancel'));
}

function _domain_tweets_get_query_edit_form($query_id, $readonly = FALSE) {

  if ($query_id == 'new') {
    $result = array(
        'query_name' => 'New Query',
        'query_type' => 0,
        'query_term' => 'insert term here',
        );
  }
  else {
    $query = db_query('
        SELECT query_name, query_type, query_term
        FROM {domain_tweets}
        WHERE query_id = %d;', $query_id);
    $result = db_fetch_array($query);
  }

  $query_term = array(
      '#type' => 'textfield',
      '#title' => t('Twitter username or search term'),
      '#default_value' => $result['query_term'],
      '#size' => 20,
      '#disabled' => $readonly,
      );

  if (!$readonly) {
    $suffix = t("<strong>Note: Twitter search results are not filtered and may contain profanity.</strong>");
  }

  $query_name = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $result['query_name'],
      '#size' => 20,
      '#disabled' => $readonly,
      );

  $query_type = array(
      '#type' => 'select',
      '#title' => t('Use as'),
      '#options' => array(
        DT_SEARCH_TERM => t('Twitter search'),
        DT_USERNAME => t('Twitter username'),
        ),
      '#default_value' => $result['query_type'],
      '#disabled' => $readonly,
      '#suffix' => $suffix,
      );

  $active_sites = array(
      '#type' => 'fieldset',
      '#title' => t('Sites which use this query'),
      '#collapsible' => TRUE,
      '#collapsed'   => $readonly,
      );

  $sites_query = db_query("
      SELECT {domain}.domain_id, sitename, query_id
      FROM {domain} 
      INNER JOIN {domain_tweets}
      ON {domain}.domain_id = {domain_tweets}.domain_id 
      ORDER BY sitename ASC;");

  $sites_selected = array();
  while ($sites_result = db_fetch_array($sites_query)) {
    if ($readonly && $sites_result['query_id'] == $query_id) {
      $sites[ $sites_result['domain_id'] ] = $sites_result['sitename'];
    }
    else if (!$readonly){
      $sites[ $sites_result['domain_id'] ] = $sites_result['sitename'];
      if ($sites_result['query_id'] == $query_id) {
        $sites_selected[$sites_result['domain_id']] = $sites_result['domain_id'];
      }
    }
  }

  $active_sites['site_list'] = array(
      '#type' => 'select',
      '#multiple' => 'true',
      '#options' => $sites,
      '#disabled' => $readonly || $query_id == '0',
      '#default_value' => $sites_selected,
      );

  if ($readonly) {
    $submit_value = t('Edit').' '.$result['query_name'];
    $submit_callback = array('domain_tweets_view_all_form_submit');
  }
  else {
    $submit_value = t('Submit');
    $submit_callback = array('domain_tweets_edit_form_submit');
  }

  $submit = array(
      '#type' => 'submit',
      '#value' => $submit_value,
      '#name' => $query_id,
      '#submit' => $submit_callback,
      );

  $query_id_form = array(
      '#type' => 'value',
      '#value' => $query_id,
      );

  if ($readonly) {
    $form = array(
        '#type' => 'fieldset',
        '#title' => $result['query_name'],
        '#prefix' => '<div class="container-inline">',
        '#suffix' => '</div>',
        );
    $form['query_term']  = $query_term;
    $form['query_type']  = $query_type;
    $form['query_id']    = $query_id_form;
    $form['active_sites'] = $active_sites;
    $form['submit']       = $submit;
    if ($query_id != '0') {
      $form['delete'] = array(
          '#value' => "<a href='domain_tweets/delete/".
          $query_id."'>".t('Delete').' '.
          $result['query_name']."</a>",
          );
    }
  }
  else {
    $form['query_name']   = $query_name;
    $form['query_term']   = $query_term;
    $form['query_type']   = $query_type;
    $form['query_id']     = $query_id_form;
    $form['active_sites'] = $active_sites;
    $form['submit']       = $submit;

  }

  return $form;
}


function domain_tweets_view_all_form_validate($form, &$form_state) {
  if (!$form_state['new']) {
    $query_id = $form_state['query_id']['#value'];
    if (!_domain_tweets_query_id_is_valid($query_id) && $query_id != 'new') {
      form_set_error('query_id', "Please choose a valid query id.");
    }
  }
  return;
}

function domain_tweets_disable_all_submit($form, &$form_state) {
  if ($form_state['values']['disable']) {
    variable_set('domain_tweets_disable', 1);
    drupal_set_message(t('Domain Tweets have been disabled for all neighborhoods.'));
  }
  else {
    variable_set('domain_tweets_disable', 0);
    drupal_set_message(t('Domain Tweets have been enabled for all neighborhoods.'));
  }
  $form_state['redirect'] = 'admin/settings/domain_tweets';
}

function domain_tweets_view_all_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Create a new query.')) {
    $form_state['redirect'] = 'admin/settings/domain_tweets/edit/new';
  }
  else {
    $query_id = $form_state['clicked_button']['#name'];
    $form_state['redirect'] = "admin/settings/domain_tweets/edit/$query_id";
  }
}


function domain_tweets_edit_form_validate($form, &$form_state) {
  #validate search term
  #validate search type
  #validate list of sites
  #validate that query_id is new or valid
}

function domain_tweets_edit_form_submit($form, &$form_state) {
  #if an existing site is being changed, add a verification step
  $query_id   = $form_state['values']['query_id'];
  $query_name = $form_state['values']['query_name'];
  $query_type = $form_state['values']['query_type'];
  $query_term = $form_state['values']['query_term'];

  if ($query_id == 'new') {
    $query_id = _domain_tweets_get_new_query_id();
  }

  if ($query_id == '0') {
    db_query('UPDATE {domain_tweets}
        SET query_name="%s",
        query_type=%d,
        query_term="%s"
        WHERE query_id = 0',
        $query_name, $query_type, $query_term);
    variable_set('domain_tweets_defaults', array(
          'query_name' => $query_name,
          'query_type' => $query_type,
          'query_term' => $query_term,
          ));
  }
  else {
  # Make sure that any sites that were unselected will
  # revert to the default settings.
    $defaults = variable_get('domain_tweets_defaults');
    db_query('UPDATE {domain_tweets}
        SET query_id=0,
        query_name="%s",
        query_type=%d,
        query_term="%s"
        WHERE query_id = %d',
        $defaults['query_name'], $defaults['query_type'],
        $defaults['query_term'], $query_id);
        #drupal_set_message("<pre>".print_r($form_state,true).".</pre>");

    foreach ($form_state['values']['site_list'] as $domain_id) {
      #drupal_set_message("updating domain $domain_id");
      db_query('UPDATE {domain_tweets}
          SET query_id=%d,
          query_name="%s",
          query_type=%d,
          query_term="%s"
          WHERE domain_id = %d',
          $query_id, $query_name, $query_type, $query_term, $domain_id);
    }
  }

  $form_state['redirect'] = 'admin/settings/domain_tweets';
}


function domain_tweets_delete_form_submit($form, &$form_state) {

  $old_query_id = $form_state['values']['query_id'];
  $defaults = variable_get('domain_tweets_defaults');
  $query_name = $defaults['query_name'];
  $query_type = $defaults['query_type'];
  $query_term = $defaults['query_term'];
  $domains_query = db_query('SELECT domain_id FROM {domain_tweets} WHERE query_id = %d'. $old_query_id);

  while ($domain_result = db_fetch_array($domains_query)) {
    $domain_id = $domain_result['domain_id'];
    db_query('UPDATE {domain_tweets}
        SET query_id = 0,
        query_name = "%s",
        query_type = %d,
        query_term = "%s"
        WHERE domain_id = %d',
        $query_name, $query_type, $query_term, $domain_id);
  }
  $form_state['redirect'] = 'admin/settings/domain_tweets';
  return;
}

function _domain_tweets_query_id_is_valid($query_id) {
  $result = db_result(db_query('SELECT COUNT(query_id) FROM {domain_tweets} WHERE query_id = %d', $query_id));
  return $result > 0;
}

function _domain_tweets_get_new_query_id() {
  $query = db_query('SELECT MAX(query_id) AS max FROM {domain_tweets}');
  $result = db_fetch_array($query);
  return $result['max']+1;
}



