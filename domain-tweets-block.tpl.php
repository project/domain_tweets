























<script>
<?php  #subtly different code is due to Twitter's API
if ($query_info['query_type'] == DT_SEARCH_TERM) { ?>

var tweet_url = "http://search.twitter.com/search.json?q=<?php echo $query_info['query_term']; ?>&rpp=1?callback=?";
var display_tweet = function(data, textStatus, xhr) {

  if (typeof data == 'undefined') {
    if ( $('#domain-tweets-age').html() == '' ){
      $('#domain-tweets-tweet').replaceWith('Twitter is not available.');
    }
    return;
  }

  if (data.results.length == 0) {
    $('#domain-tweets-tweet').replaceWith("Twitter didn't find any tweets containing \"<strong><?php echo $query_info['query_term']; ?></strong>\".");
  }
  else {
    $.each(data.results, function(i, tweet) {
      var url = 'http://twitter.com/' + tweet.from_user + '/status/' + tweet.id_str;
      //current format: Mon, 06 Dec 2010 19:21:00 +0000
      var time_created = tweet.created_at;
      time_created = time_created.replace(/([MTWTFSS][a-z][a-z]),/, '');
      $('#domain-tweets-tweet').replaceWith('<a href="' + url + '">' + tweet.text + '</a>');
      $('#domain-tweets-age').replaceWith( '<abbr class="field-content timeago" title="' + time_created + '">' + time_created + '</abbr>');
      $('abbr.timeago').timeago();
      return;
    });
  }
};

<?php }
  else if ($query_info['query_type'] == DT_USERNAME) { ?>

var tweet_url = "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=<?php echo $query_info['query_term']; ?>&callback=?";
var display_tweet = function(data, textStatus, xhr) {

  if (typeof data == 'undefined') {
    if ( $('#domain-tweets-age').html() == '') {
      $('#domain-tweets-tweet').replaceWith('Twitter is not available.');
    }
    return;
  }

  //I can't do error handling here because Twitter's 404 for a bad screen name
  //search prevents the display_tweet callback from being called.
  $.each(data, function(i, tweet) {
    var url = 'http://twitter.com/' + tweet.user.screen_name + '/status/' + tweet.id_str;
    //current format: Sun Dec 05 23:55:14 +0000 2010
    var time_created = tweet.created_at;
    time_created = time_created.replace(/\+0000 /,'');
    $('#domain-tweets-tweet').replaceWith('<a href="' + url + '">' + tweet.text + '</a>');
    $('#domain-tweets-age').replaceWith('<abbr class="timeago field-content" title="' + time_created + '">' + time_created + '</abbr>');
    jQuery('abbr.timeago').timeago();
    return;
  });
};

<?php } ?>

var domain_tweets_xhr = $.ajax({
  type: "GET",
  dataType: "jsonp",
  url: tweet_url,
  success: display_tweet
});

setTimeout(function(){display_tweet();}, 1500);


</script>

<div class="block" id="block-Domain-Tweet-block_1">
  <div class="block-inner">
    <h2 class="title">A Recent Tweet</h2>
    <div class="content view-display-id-block_1">
      <div class="view-content">
        <div class="views-row-1">
          <div class="views-field-url">
            <span class="field-content" id="domain-tweets-tweet">
             loading...
            </span>
          </div>
          <div class="views-field-created">
            <span class="field-content" id="domain-tweets-age"></span>
          </div>
        </div>
      </div>
      <div class="view-footer">
        <div class="follow-us">
          <a target="_blank" href="<?php
global $_domain;
$neighborhood = hyperlocal_get_neighborhood_by_subdomain($_domain['subdomain']);
echo $neighborhood->field_twitter_url[0]['value'];
          ?>"></a>
        </div>
      </div>
    </div>
  </div>
</div>
